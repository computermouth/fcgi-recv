#include "fcgi_stdio.h"
#include <stdlib.h>

int main(void){
	
	while(FCGI_Accept() >= 0){
		printf("Content-type: text/html\r\nStatus: 200 OK\r\n\r\n");
		
		FILE * fp;		
		fp = fopen("/tmp/status", "r");
		
		char c;
		while((c = getc(fp)) != EOF){
			printf("%c", c);
		}
		
		fclose(fp);
	}

	return 0;
}
