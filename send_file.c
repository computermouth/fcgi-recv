#include "fcgi_stdio.h"
#include <stdlib.h>
#include <string.h>

int main(void){
	
	while(FCGI_Accept() >= 0){
		printf("Content-type: text/html\r\nStatus: 200 OK\r\n\r\n");
		
		char* qs = getenv("QUERY_STRING");		
		char filename[100] = "/tmp/";
		strcat(filename, qs);
				
		FILE * fp;
		fp = fopen(filename, "w");
		fclose(fp);
		
		fp = fopen(filename, "a");
		
		char c;
		while((c = getchar()) != EOF){
			fprintf(fp, "%c", c);
		}
		
		fclose(fp);
		
	}

	return 0;
}
