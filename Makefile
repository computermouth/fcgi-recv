
all: hello echo set_status get_status send_file get_file

clean:
	rm hello echo set_status get_status send_file get_file

hello:
	gcc hello.c -o hello -lfcgi

echo:
	gcc echo.c -o echo -lfcgi

set_status:
	gcc set_status.c -o set_status -lfcgi

get_status:
	gcc get_status.c -o get_status -lfcgi

send_file:
	gcc send_file.c -o send_file -lfcgi

get_file:
	gcc get_file.c -o get_file -lfcgi

